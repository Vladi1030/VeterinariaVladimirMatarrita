/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Vacuna;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class VacunaDAO {

    public VacunaDAO() {
    }
    
    public boolean insertar(Vacuna v) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into vacuna(descripcion, precio, id_mascota, fecha) values (?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, v.getDescripcion());
            smt.setInt(2, v.getPrecio());
            smt.setInt(3, v.getId_dueno());
            smt.setDate(4, new java.sql.Date(v.getFecha().getTime()));
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    public LinkedList<Vacuna> cargarVacunas() {
        LinkedList<Vacuna> personas = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from vacuna";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet rs = smt.executeQuery();
            while (rs.next()) {
                personas.add(cargar(rs));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return personas;
    }
    
    private Vacuna cargar(ResultSet rs) throws SQLException {
        Vacuna v = new Vacuna();
        v.setFecha(rs.getDate("fecha"));
        return v;
    }
}
