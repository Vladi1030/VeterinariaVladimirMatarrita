/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Foto;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Vladimir
 */
public class FotoDAO {
     public void insertar(Foto foto) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into imagen(descripcion, foto, id_dueno) values(?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, foto.getDescripcion());
            smt.setInt(3, foto.getId_dueno());
            FileInputStream fis = new FileInputStream(foto.getFoto());
            smt.setBinaryStream(2, fis, (int) foto.getFoto().length());
            smt.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public Foto cargarID(int id) {
        Foto f = null;
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from foto where id = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, id);
            ResultSet rs = smt.executeQuery();
            if (rs.next()) {
                f = cargar(rs);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return f;
    }

    private Foto cargar(ResultSet rs) throws SQLException {
        Foto f = new Foto();
        f.setId(rs.getInt("id"));
        f.setDescripcion(rs.getString("descripcion"));
        File file = new File("");
        byte[] imgBytes = rs.getBytes("foto"); //instead of null, specify your bytes here. 
        File outputFile = new File("imagenes/" + f.getId());
        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            outputStream.write(imgBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        f.setFoto(file);
        return f;
    }
}
