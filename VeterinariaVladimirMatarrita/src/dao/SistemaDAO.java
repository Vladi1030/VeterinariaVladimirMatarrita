/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Sistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class SistemaDAO {

    public SistemaDAO() {
    }
    
    public boolean insertar(Sistema s) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into sistema(nombre, telefono, direccion, impuesto) values (?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, s.getNombre());
            smt.setInt(2, s.getTelefono());
            smt.setString(3, s.getDireccion());
            smt.setInt(4, Integer.parseInt(s.getImpuesto()));
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean actualizar(Sistema s) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "update sistema set nombre = ?, telefono = ?, direccion = ?, impuesto = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, s.getNombre());
            smt.setInt(2, s.getTelefono());
            smt.setString(3, s.getDireccion());
            smt.setFloat(4, Float.parseFloat(s.getImpuesto()));
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public LinkedList<Sistema> cargarSistemas() throws Exception {
        LinkedList<Sistema> sistemas = new LinkedList();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from sistema";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet rs = smt.executeQuery();
            while (rs.next()) {
               sistemas.add(cargar(rs));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return sistemas;
    }
    
    private Sistema cargar(ResultSet rs) throws SQLException {
        Sistema s = new Sistema();
        s.setNombre(rs.getString("nombre"));
        s.setDireccion(rs.getString("direccion"));
        s.setImpuesto(String.valueOf(rs.getInt("impuesto")));
        s.setTelefono(rs.getInt("telefono"));
        return s;
    }
}
