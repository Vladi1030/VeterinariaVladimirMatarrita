/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;


/**
 *
 * @author Vladimir
 */
public class UsuarioDAO {
   public boolean insertar(Usuario persona) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into usuario(cedula, nombre, apellidos, correo, genero, telefono,"
                    + "direccion, login, contrasenna, estado, tipo_de_usuario) values (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, Integer.parseInt(persona.getCedula()));
            smt.setString(2, persona.getNombre());
            smt.setString(3, persona.getApellidos());
            smt.setString(4, persona.getCorreo());
            smt.setObject(5, persona.getGenero());
            smt.setObject(6, persona.getTelefono());
            smt.setObject(7, persona.getDireccion());
            smt.setObject(8, persona.getLogin());
            smt.setObject(9, persona.getContrasena());
            smt.setObject(10, persona.isEstado());
            smt.setObject(11, persona.isTipoUsuario());
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public LinkedList<Usuario> cargarPersonas() {
        LinkedList<Usuario> personas = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from usuario";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet rs = smt.executeQuery();
            while (rs.next()) {
                personas.add(cargar(rs));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return personas;
    }

    public Usuario buscarCedula(String cedula) {
        Usuario p = null;
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from usuario where cedula = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, Integer.parseInt(cedula));
            ResultSet rs = smt.executeQuery();
            if (rs.next()) {
                p = cargar(rs);
            }
        } catch (Exception ex) {
            System.out.println("entró");
            System.out.println(ex.getMessage());
        }
        return p;
    }

    public boolean eliminar(int id) {
        try (Connection con = Conexion.conexion()) {
            String sql = "delete from persona where id = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, id);
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario p = new Usuario();
        p.setId(rs.getInt("id"));
        p.setCedula(rs.getString("cedula"));
        p.setNombre(rs.getString("nombre"));
        p.setApellidos(rs.getString("apellidos"));
        p.setContrasena(rs.getString("contrasenna"));
        p.setCorreo(rs.getString("correo"));
        p.setTelefono(Integer.parseInt(rs.getString("telefono")));
        p.setDireccion(rs.getString("direccion"));
        p.setLogin(rs.getString("login"));
        p.setEstado(rs.getBoolean("estado"));
        p.setTipoUsuario(rs.getBoolean("tipo_de_usuario"));
        p.setGenero(rs.getString("genero").charAt(0));
        return p;
    }

    public boolean actualizar(Usuario persona) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "update persona "
                    + "	set cedula=?, nombre=?, apellido_uno=?, apellido_dos=?, genero=?, fecha_nacimiento=? "
                    + "	where id=?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, persona.getCedula());
            smt.setString(2, persona.getNombre());
            smt.setString(3, persona.getApellidos());
            smt.setString(5, String.valueOf(persona.getGenero()));
            smt.setInt(7, persona.getId());
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public Usuario buscarID(int id) {
        Usuario p = null;
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from persona where id = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, id);
            ResultSet rs = smt.executeQuery();
            if (rs.next()) {
                p = cargar(rs);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
}
