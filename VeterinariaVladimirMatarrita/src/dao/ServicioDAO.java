/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Mascota;
import entidades.Servicio;
import entidades.Vacuna;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class ServicioDAO {

    public ServicioDAO() {
    }
    
    public boolean registrarVacuna(String descripcion, int precio) throws Exception{
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into vacuna(descripcion, precio) values (?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, descripcion);
            smt.setInt(2, precio);
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    public boolean registrarTratamiento(String descripcion, int precio) throws Exception{
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into tratamiento(descripcion, precio) values (?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, descripcion);
            smt.setInt(2, precio);
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    public boolean registrarServicio(String descripcion, int precio) throws Exception{
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into servicio(descripcion, precio) values (?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, descripcion);
            smt.setInt(2, precio);
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public LinkedList<String> servicios() {
        LinkedList<String> s = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from servicio";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet rs = smt.executeQuery();
            while (rs.next()) {
                s.add(rs.getString("descripcion"));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return s;
    }

    public LinkedList<String> tratamiento() {
        LinkedList<String> t = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from tratamiento";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet rs = smt.executeQuery();
            while (rs.next()) {
                t.add(rs.getString("descripcion"));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return t;
    }

    public LinkedList<String> vacuna() {
        LinkedList<String> v = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from tipo_de_vacuna";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet rs = smt.executeQuery();
            while (rs.next()) {
                v.add(rs.getString("descripcion"));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return v;
    }
    
    public boolean insertar(Vacuna v) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into servicio(descripcion, precio, id_mascota, fecha) values (?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, v.getDescripcion());
            smt.setInt(2, v.getPrecio());
            smt.setInt(3, v.getId_dueno());
            smt.setDate(4, new java.sql.Date(v.getFecha().getTime()));
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    public boolean insertar(Servicio v) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into servicio(descripcion, precio, id_mascota, fecha) values (?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, v.getDescripcion());
            smt.setInt(2, v.getPrecio());
            smt.setInt(3, v.getId_dueno());
            smt.setDate(4, new java.sql.Date(v.getFecha().getTime()));
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
