/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Mascota;
import entidades.Usuario;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class MascotaDAO {

    public MascotaDAO() {
    }

    public boolean insertar(Mascota mascota) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into mascota(nombre, fecha_ingreso, fecha_nacimiento, sexo, tipo, raza,"
                    + "color, peso, observaciones, id_duenno) values (?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, mascota.getNombre());
            smt.setDate(2, new java.sql.Date(mascota.getFecha_ingreso().getTime()));
            smt.setDate(3, new java.sql.Date(mascota.getFecha_nacimiento().getTime()));
            smt.setObject(4, mascota.getSexo());
            smt.setString(5, mascota.getTipo());
            smt.setString(6, mascota.getRaza());
            smt.setString(7, mascota.getColor());
            smt.setInt(8, mascota.getPeso());
            smt.setString(9, mascota.getObservaciones());
            smt.setInt(10, mascota.getDuenno());
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public LinkedList<Mascota> cargarMascotas() {
        LinkedList<Mascota> mascotas = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from mascota";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet rs = smt.executeQuery();
            while (rs.next()) {
                mascotas.add(cargar(rs));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return mascotas;
    }

    public Mascota buscarCedula(String nombre, int id) {
        Mascota m = null;
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from mascota where id_duenno = ? and nombre = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, id);
            smt.setString(2, nombre);
            ResultSet rs = smt.executeQuery();
            if (rs.next()) {
                m = cargar(rs);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return m;
    }
    
    public Mascota buscar(int id) {
        Mascota m = null;
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from mascota where id_duenno = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, id);
            ResultSet rs = smt.executeQuery();
            if (rs.next()) {
                m = cargar(rs);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return m;
    }

    public boolean eliminar(int id) {
        try (Connection con = Conexion.conexion()) {
            String sql = "delete from mascota where id = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, id);
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }

    private Mascota cargar(ResultSet rs) throws SQLException {
        Mascota m = new Mascota();
        m.setId(rs.getInt("id"));
        m.setNombre(rs.getString("nombre"));
        m.setFecha_ingreso(rs.getDate("fecha_ingreso"));
        m.setFecha_nacimiento(rs.getDate("fecha_nacimiento"));
        m.setSexo(rs.getString("sexo").charAt(0));
        m.setTipo(rs.getString("tipo"));
        m.setRaza(rs.getString("raza"));
        m.setColor(rs.getString("color"));
        m.setPeso(rs.getInt("peso"));
        m.setObservaciones(rs.getString("observaciones"));
        m.setDuenno(rs.getInt("id_duenno"));
        return m;
    }

    public boolean actualizar(Mascota mascota) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "update mascota"
                    + "	set nombre=?, fecha_ingreso = ?, fecha_nacimiento = ?, sexo = ?, tipo = ?, raza = ?, color = ?"
                    + ", peso = ?, observaciones = ?, id_duenno where id=?";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, mascota.getNombre());
            smt.setDate(2, (Date) mascota.getFecha_ingreso());
            smt.setDate(3, (Date) mascota.getFecha_nacimiento());
            smt.setString(4, String.valueOf(mascota.getSexo()));
            smt.setString(5, mascota.getTipo());
            smt.setString(6, mascota.getRaza());
            smt.setString(7, mascota.getColor());
            smt.setInt(8, mascota.getPeso());
            smt.setString(9, mascota.getObservaciones());
            smt.setInt(5, mascota.getId());
            return smt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
