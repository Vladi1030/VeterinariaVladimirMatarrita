/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.File;

/**
 *
 * @author Vladimir
 */
public class Foto {
    private int id;
    private String descripcion;
    private File foto;
    private int id_dueno;

    public Foto() {
    }

    public Foto(int id, String descripcion, File foto) {
        this.id = id;
        this.descripcion = descripcion;
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getId_dueno() {
        return id_dueno;
    }

    public void setId_dueno(int id_dueno) {
        this.id_dueno = id_dueno;
    }

    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public File getFoto() {
        return foto;
    }

    public void setFoto(File foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "Foto{" + "id=" + id + ", descripcion=" + descripcion + '}';
    }
}