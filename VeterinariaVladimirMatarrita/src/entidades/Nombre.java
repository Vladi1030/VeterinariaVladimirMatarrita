/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author Vladimir
 */
public class Nombre {
    private String nombre;
    private String apellido;

    public Nombre(String nombre, String apellidoUno) {
        this.nombre = nombre;
        this.apellido = apellidoUno;
    }

    public Nombre() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellido;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellido = apellidoUno;
    }
}
