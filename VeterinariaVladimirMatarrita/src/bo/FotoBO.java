/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import dao.FotoDAO;
import entidades.Foto;
import java.awt.Image;
import java.io.File;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Vladimir
 */
public class FotoBO {
    public void guardar(Foto foto) throws Exception {
        if (foto.getDescripcion().isEmpty()) {
            throw new Exception("Favor agregar una descripción a la foto");
        }
        if (foto.getFoto() == null) {
            throw new Exception("Favor seleccionar una foto");
        }
        FotoDAO fdao = new FotoDAO();
        fdao.insertar(foto);
    }

    public Icon cargarFoto(int id, int width, int height) throws Exception {
        if (id <= 0) {
            return null;
        }
        FotoDAO fdao = new FotoDAO();
        Foto foto = fdao.cargarID(id);
        ImageIcon img = new ImageIcon(foto.getFoto().getAbsolutePath() + "/imagenes/" + foto.getId());
        Image imagen = img.getImage();
        Image newimg = imagen.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        img = new ImageIcon(newimg);
        File f = new File(foto.getFoto().getAbsolutePath() + "/imagenes/" + foto.getId());
        f.delete();
        return img;
    }
}
