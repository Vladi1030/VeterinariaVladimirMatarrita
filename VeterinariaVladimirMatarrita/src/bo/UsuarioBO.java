/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import dao.UsuarioDAO;
import entidades.Usuario;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class UsuarioBO {
    private UsuarioDAO pdao;

    public UsuarioBO() {
        pdao = new UsuarioDAO();
    }
    
    public boolean registrar(Usuario persona) throws Exception {
        if (persona.getCedula().isEmpty()) {
            throw new Exception("Cédula requerida");
        }
        if (persona.getNombre().isEmpty()) {
            throw new Exception("Nombre requerido");
        }
        if (persona.getApellidos().isEmpty()) {
            throw new Exception("Primer apellido requerido");
        }
        if (persona.getGenero() != 'M' && persona.getGenero() != 'F') {
            throw new Exception("Género requerido");
        }
        UsuarioDAO pdao = new UsuarioDAO();
        if (persona.getId() > 0) {
            return pdao.actualizar(persona);
        } else {
            return pdao.insertar(persona);
        }
    }

    public LinkedList<Usuario> buscar(String ced) {
        UsuarioDAO pdao = new UsuarioDAO();
        if (ced.isEmpty()) {
            return pdao.cargarPersonas();
        } else {
            Usuario p = pdao.buscarCedula(ced);
            LinkedList<Usuario> temp = new LinkedList<>();
            temp.add(p);
            return temp;
        }
    }

    public boolean eliminar(Usuario p) throws Exception {
        if (p.getId() <= 0) {
            throw new Exception("Favor seleccionar una persona");
        }
        return pdao.eliminar(p.getId());
    }

    public boolean verificarLogin(String Login, String password) {
        LinkedList<Usuario> u = pdao.cargarPersonas();
        for (int i = 0; i < u.size(); i++) {
            if(u.get(i).getLogin().equals(Login)){
                if(u.get(i).getContrasena().equals(password)){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean verificarTipo(String usuario) {
        LinkedList<Usuario> u = pdao.cargarPersonas();
        for (int i = 0; i < u.size(); i++) {
            if(u.get(i).getLogin().equals(usuario)){
                if(u.get(i).isTipoUsuario()){
                    return true;
                }
            }
        }
        return false;
    }

    public LinkedList<Usuario> cargarPersonas() {
        return pdao.cargarPersonas();
    }

    public Usuario verificarUsuario(int ced) {
        LinkedList<Usuario> u = pdao.cargarPersonas();
        for (int i = 0; i < u.size(); i++) {
            if(u.get(i).getCedula().equals(String.valueOf(ced))){
                return u.get(i);
            }
        }
        return null;
    }

    public Usuario usuario(String login) {
        LinkedList<Usuario> u = pdao.cargarPersonas();
        for (int i = 0; i < u.size(); i++) {
            if(u.get(i).getLogin().equals(login)){
                return u.get(i);
            }
        }
        return null;
    }

    public Usuario buscarCedula(String ced) {
       return pdao.buscarCedula(ced);
    }
}
