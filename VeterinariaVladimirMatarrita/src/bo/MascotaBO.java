/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import dao.MascotaDAO;
import entidades.Foto;
import entidades.Mascota;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class MascotaBO {

    private MascotaDAO mdao;

    public MascotaBO() {
        mdao = new MascotaDAO();
    }

    public boolean registrar(Mascota mascota) throws Exception {
        if (mascota.getDuenno() == 0) {
            throw new Exception("id del dueño requerido");
        }
        if (mascota.getNombre().isEmpty()) {
            throw new Exception("Nombre requerido");
        }
        if (mascota.getRaza().isEmpty()) {
            throw new Exception("Raza requerida");
        }
        if (mascota.getSexo() != 'M' && mascota.getSexo() != 'H') {
            throw new Exception("Sexo requerido");
        }
        if (mascota.getPeso() == 0) {
            throw new Exception("Raza requerida");
        }
        if (mascota.getTipo().isEmpty()) {
            throw new Exception("Raza requerida");
        }
        mdao = new MascotaDAO();
        if (mascota.getId() > 0) {
            return mdao.actualizar(mascota);
        } else {
            System.out.println("yes");
            return mdao.insertar(mascota);
        }
    }

    public LinkedList<Mascota> buscar(String nombre, int id) {
        if (nombre.isEmpty()) {
            return mdao.cargarMascotas();
        } else {
            Mascota p = mdao.buscarCedula(nombre, id);
            LinkedList<Mascota> temp = new LinkedList<>();
            temp.add(p);
            return temp;
        }
    }

    public LinkedList<Mascota> buscarId(int id) {
        Mascota p = mdao.buscar(id);
        LinkedList<Mascota> temp = new LinkedList<>();
        temp.add(p);
        return temp;
    }

    public boolean eliminar(Mascota p) throws Exception {
        if (p.getId() <= 0) {
            throw new Exception("Favor seleccionar una persona");
        }
        return mdao.eliminar(p.getId());
    }

    public LinkedList<Mascota> cargarMascotas() {
        return mdao.cargarMascotas();
    }
}
