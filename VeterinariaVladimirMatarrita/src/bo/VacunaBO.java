/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import dao.VacunaDAO;
import entidades.Vacuna;
import java.util.Date;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class VacunaBO {

    private VacunaDAO vdao;

    public VacunaBO() {
        vdao = new VacunaDAO();
    }

    public boolean registrar(Vacuna v) throws Exception {
        if (v.getDescripcion().isEmpty()) {
            throw new Exception("Descripción requerida");
        }
        if (v.getPrecio() == 0) {
            throw new Exception("Precio requerido");
        }
        if (v.getId_dueno() == 0) {
            throw new Exception("Id de la mascota requerido");
        }
        if (v.getFecha().equals("")) {
            throw new Exception("Fecha requerida");
        }
        VacunaDAO vdao = new VacunaDAO();
        return vdao.insertar(v);
    }
    
    public int validarFecha(Date fecha){
        LinkedList<Vacuna> lv = vdao.cargarVacunas();
        int contador = 0;
        for (int i = 0; i < lv.size(); i++) {
            if(fecha.equals(lv.get(i).getFecha())){
                contador++;
            }
        }
        return contador;
    }

}