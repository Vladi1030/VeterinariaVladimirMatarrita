/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import dao.SistemaDAO;
import entidades.Sistema;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class SistemaBO {

    SistemaDAO sdao;

    public SistemaBO() {
        sdao = new SistemaDAO();
    }

    public boolean insertar(Sistema s) throws Exception {
        if (s.getNombre().isEmpty()) {
            throw new Exception("Nombre requerido");
        }
        if (s.getDireccion().isEmpty()) {
            throw new Exception("Dirección requerida");
        }
        if (s.getImpuesto().isEmpty()) {
            throw new Exception("Impuesto requerido");
        }
        if (s.getTelefono() == 0) {
            throw new Exception("Teléfono requerido");
        }
        LinkedList<Sistema> sis = sdao.cargarSistemas();
        if (sis.isEmpty()) {
            return sdao.insertar(s);
        } else {
            return sdao.actualizar(s);
        }
    }

    public LinkedList<Sistema> cargarDatos() throws Exception {
        return sdao.cargarSistemas();
    }

}
