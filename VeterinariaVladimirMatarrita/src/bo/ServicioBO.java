/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import dao.Conexion;
import dao.ServicioDAO;
import dao.VacunaDAO;
import entidades.Servicio;
import entidades.Vacuna;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class ServicioBO {
    
    private ServicioDAO sdao;

    public ServicioBO() {
        sdao = new ServicioDAO();
    }
    
    public LinkedList<String> tratamiento(){
        return sdao.tratamiento();
    }
    
    public LinkedList<String> vacuna(){
        return sdao.vacuna();
    }
    
    public LinkedList<String> servicios(){
        return sdao.servicios();
    }
    
    public boolean registrarVacuna(String descripcion, int precio) throws Exception{
        return sdao.registrarVacuna(descripcion, precio);
    }
    
    public boolean registrarTratamiento(String descripcion, int precio) throws Exception{
        return sdao.registrarTratamiento(descripcion, precio);
    }
    
    public boolean registrarServicio(String descripcion, int precio) throws Exception{
        return sdao.registrarServicio(descripcion, precio);
    }
    private VacunaDAO vdao;

        public boolean registrar(Servicio v) throws Exception {
        if (v.getDescripcion().isEmpty()) {
            throw new Exception("Descripción requerida");
        }
        if (v.getPrecio() == 0) {
            throw new Exception("Precio requerido");
        }
        if (v.getId_dueno() == 0) {
            throw new Exception("Id de la mascota requerido");
        }
        if (v.getFecha().equals("")) {
            throw new Exception("Fecha requerida");
        }
        return sdao.insertar(v);
    }
    
    public int validarFecha(Date fecha){
        LinkedList<Vacuna> lv = vdao.cargarVacunas();
        int contador = 0;
        for (int i = 0; i < lv.size(); i++) {
            if(fecha.equals(lv.get(i).getFecha())){
                contador++;
            }
        }
        return contador;
    }

}
